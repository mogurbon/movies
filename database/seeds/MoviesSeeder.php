<?php

use Illuminate\Database\Seeder;

class MoviesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('movies')->insert([
            'name' => 'Rambo 1',
            'year' => 1996,
            'genre_id' => 1
        ]);
        DB::table('movies')->insert([
            'name' => 'Rambo 2',
            'year' => 1997,
            'genre_id' => 1
        ]);
        DB::table('movies')->insert([
            'name' => 'Rambo 3',
            'year' => 1998,
            'genre_id' => 1
        ]);

    }
}
