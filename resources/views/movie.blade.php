<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link href="{{ asset('css/template.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bulma.css') }}" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div id="app">

        <header>

            <nav class="navbar" role="navigation" aria-label="dropdown navigation">
                <div class="navbar-brand">
                    <a class="navbar-item" href="https://bulma.io">
                        <img src="https://s3-us-west-1.amazonaws.com/nearsoft-com-media/uploads/2018/06/nearsoft-aic.svg" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28">
                    </a>
                    <div class="navbar-burger burger" data-target="navbarExampleTransparentExample">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div id="navbarExampleTransparentExample" class="navbar-menu">
                    <div class="navbar-end">


                        <div class="navbar-item has-dropdown is-hoverable">
                            <a class="navbar-link" href="{{url("/")}}">
                                Movies
                            </a>

                            <div class="navbar-dropdown">
                                <a class="navbar-item" href="{{url("/newmovie")}}">
                                    New
                                </a>

                            </div>
                        </div>
                        <div class="navbar-item has-dropdown is-hoverable">
                            <a class="navbar-link" href="{{url("/actors")}}">
                                Actors
                            </a>

                            <div class="navbar-dropdown">
                                <a class="navbar-item" href="{{url("/newactor")}}">
                                    New
                                </a>

                            </div>
                        </div>

                    </div>
                </div>
            </nav>
        </header>
        <content>
            <section class="section">
        <div class="container">
            @yield('content')
        </div>
            </section>
        </content>

        <footer></footer>
    </div>
</body>
<script src="{{ asset('js/app.js') }}"></script>
</html>