
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

Vue.component('movies', require('./components/Movies.vue'));
Vue.component('newmovies', require('./components/NewMovies.vue'));
Vue.component('editmovie', require('./components/EditMovie.vue'),{props:['movie_id']});
Vue.component('editactor', require('./components/EditActor.vue'));//{props:['actor_id']}
Vue.component('actors', require('./components/Actors.vue'));
Vue.component('newactor', require('./components/NewActor.vue'));


const app = new Vue({
    el: '#app'
});
