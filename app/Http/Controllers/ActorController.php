<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actor;
use Illuminate\Support\Facades\DB;

class ActorController extends Controller
{
    public function index(){
        return view('content.actor');
    }
    public function getActors(){
        return Actor::all();
    }
    public function deleteActor($actor_id){
        return Actor::destroy($actor_id);
    }
    public function newActor(){
        return view('content.newactor');
    }
    public function store(Request $request){
        try{
            DB::beginTransaction();
            $actor = new Actor();
            $actor->name =  $request->input('name');
            $actor->save();
            DB::commit();
            $response = ['object' => null, 'error' => false, 'message' => 'Actor Stored'];
        }catch (\Exception $e){
            DB::rollback();
            $response = ['object' => null, 'error' => true, 'message' => $e->getMessage()];
        }
        return $response;

    }
    public function editActor($id){
        return view('content.editactor',['actor_id' => $id]);
    }
    public function getActor($actor_id){
        return Actor::find($actor_id);
    }
    public function updateActor(Request $request,$actor_id){
        try{
            DB::beginTransaction();
            $actor = Actor::find($actor_id);
            $actor->name = $request->input('name');
            $actor->save();
            DB::commit();
            $response = ['object' => null, 'error' => false, 'message' => 'Actor Updated'];
        }catch (\Exception $e){
            DB::rollback();
            $response = ['object' => null, 'error' => true, 'message' => $e->getMessage()];
        }
        return $response;
    }
}
