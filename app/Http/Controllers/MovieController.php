<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Actor;
use Illuminate\Support\Facades\DB;

class MovieController extends Controller
{
    public function index(){
        return view('content.content');
    }
    public function getMovie($movie_id){
        return Movie::with('actors')->find($movie_id);

    }
    public function getMovies(){
        return Movie::with('genre')->get();
    }
    public function deleteMovie($movie_id){
        return Movie::destroy($movie_id);
    }

    public function newMovie(){
        return view('content.newmovie');
    }
    public function store(Request $request){
        try{
            DB::beginTransaction();
            $movie = new Movie();
            $movie->name =  $request->input('name');
            $movie->year = $request->input('year');
            $movie->genre_id = $request->input('genre_id');
            $movie->save();
            DB::commit();
            $response = ['object' => null, 'error' => false, 'message' => 'Movie Stored'];
        }catch (\Exception $e){
            DB::rollback();
            $response = ['object' => null, 'error' => true, 'message' => $e->getMessage()];
        }
        return $response;


    }
    public function editMovie($id){
        return view('content.editmovie',['movie_id' => $id]);
    }
    public function updateMovie(Request $request, $movie_id){

        try{
            DB::beginTransaction();
            $movie = Movie::find($movie_id);
            $movie->name =  $request->input('name');
            $movie->year = $request->input('year');
            $movie->genre_id = $request->input('genre_id');

            $actors = $request->input('actors');

            if (isset($actors) and count($actors) > 0){
                $movie->actors()->detach();
                $movie->actors()->attach($actors);
            }
            $movie->save();
            DB::commit();
            $response = ['object' => null, 'error' => false, 'message' => 'Movie Updated'];
        }catch (\Exception $e){
            DB::rollback();
            $response = ['object' => null, 'error' => true, 'message' => $e->getMessage()];
        }

        return $response;
    }

}
