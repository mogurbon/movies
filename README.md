# README #



### What is this repository for? ###

* Test For Nearsoft admision


### How do I get set up? ###


* Configuration : 

      npm install

      php artisan migrate:refresh --seed
      
      php artisan serve
* Dependencies: 

    npm 
   
    PHP >= 7.1.3.
    
    OpenSSL PHP Extension.
    
    PDO PHP Extension.
    
    Mbstring PHP Extension.
    
    Tokenizer PHP Extension.
    
    XML PHP Extension.
    
    Ctype PHP Extension.
    
    JSON PHP Extension.

* Database configuration:

   create database an set it on .env 
* tests: 

  run  ./vendor/bin/phpunit


