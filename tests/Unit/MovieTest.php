<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class MovieTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    use DatabaseTransactions;

    public function testGetMovies(){


        $response = $this->get('/movies');

        $response->assertStatus(200);
    }
    public function testGetMovie(){

        $response = $this->json('GET', '/movies', ['id' => 1]);

        $response
            ->assertStatus(200);

    }
    public function testPostMovie(){

        $response = $this->json('POST', '/movies', ['name' => 'name','year' => '1990','genre_id'=>'2']);

        $response->assertStatus(200)->assertJson([
            'object' => null,'error'=>false,'message'=>'Movie Stored'
        ]);

    }
    public function testPutMovie(){
        $response = $this->json('PUT', '/movie/1', ['name' => 'name','year' => '1990','genre_id'=>'2']);
        $response->assertStatus(200);
    }

}
