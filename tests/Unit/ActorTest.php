<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActorTest extends TestCase
{
    use DatabaseTransactions;

    public function testPutMovie(){
        $response = $this->json('PUT', '/actor/1', ['name' => 'name']);
        $response->assertStatus(200);
    }
    public function testGetActors(){
        $response = $this->get('/actors');

        $response->assertStatus(200);
    }

    public function testGetActor(){
        $response = $this->get('/actor/1');

        $response->assertStatus(200);
    }



}
