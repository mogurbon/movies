<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['prefix' => 'genres'], function() {

    Route::get('/', 'GenreController@getGenres');
});

Route::get('/', 'MovieController@index');
Route::get('/newmovie', 'MovieController@newMovie');
Route::get('/movies', 'MovieController@getMovies');
Route::delete('/movies/{id}', 'MovieController@deleteMovie');
Route::post('/movies', 'MovieController@store');
Route::get('/movies/{id}', 'MovieController@editMovie');
Route::get('/movie/{id}', 'MovieController@getMovie');
Route::put('/movie/{id}', 'MovieController@updateMovie');
Route::put('/actors/{id}', 'MovieController@updateMovie');


Route::get('/actors', 'ActorController@index');
Route::get('/actors/{id}', 'ActorController@editActor');
Route::get('/actor/{id}', 'ActorController@getActor');
Route::get('/listactors', 'ActorController@getActors');
Route::delete('/actor/{id}', 'ActorController@deleteActor');
Route::get('/newactor', 'ActorController@newActor');
Route::post('/actor', 'ActorController@store');
Route::put('/actor/{id}', 'ActorController@updateActor');



